const { info, error } = require('../libs/logger')({ scope: 'TestService' });
const storage = require('../models');


const getUser = async (user) => {
    // return {
    //     name: "test"
    // }
    const data = await storage.getUser({ user });
    info(user);
    return data;
}

module.exports = { getUser}