const texts = require('../texts');

const send = (req, res, httpCode, description) => {
    const data = {
        message: description || texts[httpCode || 400],
    };
    res.status(httpCode).send(data);
};

module.exports = { send };
