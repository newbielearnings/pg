
module.exports = {
    user: {
        in: ['params'],
        // sanitizers
        trim: true,
        customSanitizer: { options: value => String(value).toLowerCase() },
        // Validations
        isAlphanumeric: { errorMessage: 'non alpha loginID' },
        isLowercase: true,
        isLength: {
            errorMessage: 'invalid length',
            options: { min: 3, max: 10 },
        }
    }
};
