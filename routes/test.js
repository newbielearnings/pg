const schema = require('../middlewares/schema');
const testSchema = require('../schemas/test');
const testController = require('../controllers/test');

const enable = (router) => {
    router.get('/users/:user', schema.validate(testSchema), schema.parseResult, (req, res) => {
        testController.getUser(req, res);
    });
    return router;
};

module.exports ={
    enable
}
