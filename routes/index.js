const router = require('express').Router();
const testRoutes = require('./test');

testRoutes.enable(router);

module.exports = router;
