module.exports = {
    404: "Service you are trying to request doesn't exist.",
    500: 'Oops ! something went wrong, Please try again.',
    400: 'Bad request'
}