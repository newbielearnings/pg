const { info, error } = require('../libs/logger')({ scope: 'TestController' });
const testService = require('../services/test');
const fiveXX = require('../utils/4xx');


const getUser = async (req, res) => {
    const { user } = req.params;
    try {
        const data = await testService.getUser({ user });
        res.json(data);
        info('successful return')
    } catch (e) {
        error(e.message);
        fiveXX.send(req, res, 500, "Something is off, try again later")
    }
};

module.exports = { getUser}