const express = require('express');
const path = require('path');

const loaders = require('./loaders');
const { authProtections, healthRoutes, requestTransformers, envChecker, models, errorRoutes, apiRoutes } = loaders;

const app = express();
requestTransformers.load(app);
authProtections.load(app);
models.load(app);
healthRoutes.load(app);
apiRoutes.load(app);
errorRoutes.load(app);
envChecker.load(app);

module.exports = app;
