const winston = require('winston');

const { LOG_LEVEL = 'debug' } = process.env;
const { createLogger, transports, format } = winston;
const { combine, timestamp, label, printf } = format;

const formatter = printf(({ level, message, scope, timestamp }) => {
    return `[${level}] ${timestamp} [${scope}] ${message}`;
});

const logger = createLogger({
    format: combine(label({}), timestamp(), formatter),
    level: LOG_LEVEL,
    defaultMeta: {},
    transports: [new transports.Console()],
});

// Do not use arrow function, as we need to bind
function loggerMapper(meta = {}, level, message, extraData = {}) {
    logger[level](message, meta);
}

function customerLogger(meta) {
    const metaBinder = loggerMapper.bind(null, meta);
    return {
        error: metaBinder.bind(null, 'error'),
        info: metaBinder.bind(null, 'info'),
        log: metaBinder.bind(null, 'log'),
        debug: metaBinder.bind(null, 'debug'),
    };
}

module.exports = customerLogger;
