const { info, error } = require('../libs/logger')({ scope: 'ENVChecker' });

const load = () => {
    if (process.env.NODE_ENV !== 'production') {
        error('App is not running in production mode. Please export NODE_ENV to production');
    } else {
        info('production mode is up');
    }
};

module.exports = { load };
