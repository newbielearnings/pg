const models = require('../models');

const load = () => {
    models.init();
};

module.exports = { load };
