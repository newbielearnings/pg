
const {notFound, unknownError} = require('../middlewares/error');

const load = app => {
    app.use(notFound);
    app.use(unknownError);
};

module.exports = { load };