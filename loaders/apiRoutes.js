const apiRoutes = require('../routes');

const load = app => {
    app.use("/api_name/v1", apiRoutes);
};

module.exports = { load };
