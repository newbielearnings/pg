const bodyParser = require('body-parser');
const helmet = require('helmet');
const compression = require('compression');

const load = (app) => {
    app.use(helmet());
    app.use(compression());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
};

module.exports = { load };
