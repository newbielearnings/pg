const ipLogger = require('../middlewares/ipLogger');
const authorizer = require('../middlewares/authorizer');


const load = app => {
    app.use(ipLogger);
    app.use(authorizer);
};

module.exports = { load };
