const load = app => {
    app.use('/health', (req, res) => {
        res.json({ status: 'UP' });
    });
};

module.exports = { load };
