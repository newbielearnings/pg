const apiRoutes = require('./apiRoutes');
const errorRoutes = require('./errorRoutes');
const authProtections = require('./authProtections');
const healthRoutes = require('./healthRoutes');
const requestTransformers = require('./requestTransformers');
const envChecker = require('./envChecker');
const models = require('./models');

module.exports = {
    apiRoutes,
    authProtections,
    healthRoutes,
    errorRoutes,
    requestTransformers,
    envChecker,
    models,
};
