
const fourXX = require('../utils/4xx');
const fiveXX = require('../utils/4xx');

const notFound = (req, res, next) => {
    fourXX.send(null, res, 404);
}

const unknownError = (err, req, res, next) => {
    error(err.message || err);
    if (req.headersSent) {
        return next(err);
    }
    fiveXX.send(req, res, 500);
    return true;
};

module.exports = { notFound, unknownError };