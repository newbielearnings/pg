const { info, debug } = require('../libs/logger')({ scope: 'INCOMING' });

module.exports = (req, res, next) => {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    info(`Incoming Request ${req.method} ${ip} ${req.originalUrl}`);
    req.body.ip = ip;
    next();
};
