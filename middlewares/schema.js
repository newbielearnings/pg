const { validationResult, checkSchema } = require('express-validator');

const fourXX = require('../utils/4xx');
const { info } = require('../libs/logger')({ scope: 'schemaValidation' });

const parseResult = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const { loginID, ip } = req.body;
        info(`endpoint: ${req.originalUrl} error(s): ${JSON.stringify(errors.array())} `, {
            ip,
            loginID,
            endpoint: req.originalUrl,
        });
        fourXX.send(req, res, 422);
    } else {
        next();
    }
};

module.exports = {
    parseResult,
    validate: checkSchema,
};
