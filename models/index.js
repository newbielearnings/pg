const { Sequelize, QueryTypes } = require("sequelize");

const connString = "postgres://user:pass@example.com:5432/dbname";
const sequelize = new Sequelize(connString, {
    dialectOptions: {
        statement_timeout: 10000,
        idle_in_transaction_session_timeout: 10000
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000,
    },
});

const init = async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
};

const getUser = async () => {
    const users = await sequelize.query(
      "SELECT * FROM projects WHERE status = :status",
      {
        replacements: { status: "active" },
        type: QueryTypes.SELECT,
      }
    );
    return users;
  };
  
  module.exports = {
    init,
    getUser,
  };
  